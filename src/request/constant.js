export const PDF_PREVIEW_URL = 'https://www.shengtai.club/jeecg-boot/generic/web/viewer.html?file=';
export const OFFICE_PREVIEW_URL = 'https://fileview.shengtai.club/onlinePreview?officePreviewType=image&url=';
export const OFFICE_PREVIEW_TYPE = ['doc', 'docx', 'xlsx', 'xls', 'ppt', 'pptx'];
export const WORKFLOW_STATUS = { 1: '待提交', 2: '处理中', 3: '审核中', 4: '已完成', 5: '已知会' };